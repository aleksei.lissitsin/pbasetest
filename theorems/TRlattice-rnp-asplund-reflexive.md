---
uid: TRlattice-rnp-asplund-reflexive
if:
  and:
    - PRlattice: true
    - Prnp: true
    - Pasplund: true
then:
  Preflexive: true
refs:
  - doi: 10.1007/978-3-642-76724-1
    name: Banach lattices
---

An Asplund Banach lattice with RNP is reflexive.

See, e.g., Theorem 5.4.13 of {{doi:10.1007/978-3-642-76724-1}}.
