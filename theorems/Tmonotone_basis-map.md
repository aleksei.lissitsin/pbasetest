---
uid: Tmonotone_basis-map
if:
  Pmonotone_basis: true
then:
  Pmap: true
---

A Banach space with a monotone basis has the metric approximation property.
