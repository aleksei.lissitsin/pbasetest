---
uid: Tasplund-rosenthal
if:
  Pasplund: true
then:
  Prosenthal: true
---

An Asplund space does not contain an isomorphic copy of $\ell_1$.