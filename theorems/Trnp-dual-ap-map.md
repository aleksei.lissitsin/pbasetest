---
uid: Trnp-dual-ap-map
if:
  and:
    - Prnp: true
    - Pdual: true
    - Pap: true
then:
  Pmap: true
---

In a dual RNP space AP implies MAP.