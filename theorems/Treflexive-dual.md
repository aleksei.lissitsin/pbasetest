---
uid: Treflexive-dual
if:
  Preflexive: true
then:
  Pdual: true
---

A reflexive Banach space is a dual space.
