---
uid: Tinjective-quasidual
if:
  Pinjective: true
then:
  Pquasidual: true
---

Obvious from the definition.
