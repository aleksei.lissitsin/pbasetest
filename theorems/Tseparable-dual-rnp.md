---
uid: Tseparable-dual-rnp
if:
  and:
    - Pseparable: true
    - Pdual: true
then:
  Prnp: true
---

A dual separable Banach space has the Radon-Nikodym property.
