---
uid: Tunc_basis-rosenthal-pelczynski-reflexive
if:
  and:
    - Punc_basis: true
    - Prosenthal: true
    - Ppelczynski: true
then:
  Preflexive: true
refs:
  - mr: 39915
    name: Bases and reflexivity of Banach spaces.
---

A theorem of James.
