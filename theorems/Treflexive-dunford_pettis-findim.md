---
uid: Treflexive-dunford_pettis-findim
if:
  and:
  - Preflexive: true
  - Pdunford_pettis: true
then:
  Pfindim: true
---

A reflexive and Dunford-Pettis Banach space is finite-dimensional.
