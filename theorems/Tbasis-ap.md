---
uid: Tbasis-ap
if:
  Pbasis: true
then:
  Pap: true
---

A Banach space with a basis also has the approximation property.
