---
uid: Tsomewhat_reflexive-super_reflexive
if:
  and:
  - Psomewhat_reflexive: true
  - Pfindim: false
then:
  Psuper_reflexive: true
---
Trivial.
