---
uid: Tfindim-reflexive
if:
  Pfindim: true
then:
  Preflexive: true
---

A finite-dimensional Banach space is reflexive.
