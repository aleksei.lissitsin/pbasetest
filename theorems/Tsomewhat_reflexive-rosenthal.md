---
uid: Tsomewhat_reflexive-rosenthal
if:
  Psomewhat_reflexive: true
then:
  Prosenthal: true
---

$\ell_1$ is a Schur space and hence cannot contain a reflexive subspace.
