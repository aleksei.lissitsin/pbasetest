---
uid: Tfindim-basis
if:
  Pfindim: true
then:
  Pbasis: false
---

A finite-dimensional Banach space does not have a basis.
