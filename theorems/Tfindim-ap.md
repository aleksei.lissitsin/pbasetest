---
uid: Tfindim-ap
if:
  Pfindim: true
then:
  Pap: true
---

A finite-dimensional Banach space has the approximation property.
