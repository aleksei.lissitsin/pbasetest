---
uid: Pap
slug: ap
name: AP
aliases:
  - with the approximation property
---

A Banach space with the approximation property. Such spaces form a space ideal.