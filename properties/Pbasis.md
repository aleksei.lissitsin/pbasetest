---
uid: Pbasis
slug: basis
name: with basis
---

A Banach space having a Schauder basis.