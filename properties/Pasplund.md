---
uid: Pasplund
slug: asplund
name: Asplund
aliases:
  - predual of RNP spaces
---

A space such that its dual has RNP. 
Equivalently, the dual of every separable subspace is separable.
Such spaces form an injective space ideal.