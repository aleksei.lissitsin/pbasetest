---
uid: Pquasidual
slug: quasidual
name: Quasi-dual
---

A Banach space is called quasi-dual if its canonical image is complemented in its bidual.