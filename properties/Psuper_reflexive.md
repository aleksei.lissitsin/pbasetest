---
uid: Psuper_reflexive
slug: super_reflexive
name: with a reflexive subspace
---

A Banach space containing an infinite-dimensional reflexive subspace.