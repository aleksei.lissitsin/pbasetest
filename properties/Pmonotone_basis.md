---
uid: Pmonotone_basis
slug: monotone_basis
name: with a monotone basis
---

A Banach space having a monotone Schauder basis.