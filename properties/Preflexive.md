---
uid: Preflexive
slug: reflexive
name: Reflexive
refs:
  - doi: 10.2307/2039469
    name: A Renorming of Nonreflexive Banach Spaces
---

A reflexive Banach space. Note that a space is reflexive if and only if it is isometric to a dual space in every equivalent renorming. Reflexive spaces form an injective and surjective space ideal.