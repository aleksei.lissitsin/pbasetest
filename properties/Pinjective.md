---
uid: Pinjective
slug: injective
name: Injective
---

A Banach space is called injective if it is complemented in any superspace.