---
uid: PL_1
slug: L_1
name: $L_1(\mu)$
---

A Banach space isometric to $L_1(\mu)$ for some measure $\mu$.