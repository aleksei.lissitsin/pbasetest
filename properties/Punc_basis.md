---
uid: Punc_basis
slug: unc_basis
name: with an unconditional basis
---

A Banach space having an unconditional Schauder basis.