---
uid: Pgrothendieck
slug: grothendieck
name: Grothendieck
---

A Banach space is called Grothendieck if every weak-∗ convergent sequence in the dual space is weakly convergent. Such spaces form the space ideal Space$(\mathcal{Sep}^{-1} \circ \mathcal{W})$.