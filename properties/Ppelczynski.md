---
uid: Ppelczynski
slug: pelczynski
name: Pelczynski
---

A Banach space is called Pelczynski if every weakly summable series is norm summable. Such spaces form an injective and surjective space ideal.

A Banach space is Pelczynski if and only if it contains no isomorphic copy of $c_0$.

AKA: $\neg$Super<$c_0$>