---
uid: Pfindim
slug: finite-dimensional
name: Finite-dimensional Banach space
---

A Banach space having a finite basis. Such spaces form a space ideal.