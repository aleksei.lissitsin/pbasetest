---
uid: Pmap
slug: map
name: MAP
aliases:
  - with the metric approximation property
---

A Banach space with the metric approximation property.