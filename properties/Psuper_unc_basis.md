---
uid: Psuper_unc_basis
slug: super_unc_basis
name: with an unconditional basic sequence
---

A Banach space with an unconditional basic sequence.