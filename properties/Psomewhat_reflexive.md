---
uid: Psomewhat_reflexive
slug: somewhat_reflexive
name: Somewhat reflexive
---

A Banach space such that every its closed infinite-dimensional subspace contains an infinite dimensional reflexive subspace.