---
uid: Psuper_ell_p
slug: super_ell_p
name: containing an isomorphic copy of $\ell_p$
aliases:
  - Super<$\ell_p$>
---

A Banach space containing an isomorphic copy of $\ell_p$ for some $p \in (1,\infty)$.