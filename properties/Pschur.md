---
uid: Pschur
slug: schur
name: Schur
---

A Banach space is called Schur if every weakly convergent sequence is convergent. 


Such spaces form the injective space ideal Space$(\mathcal V) = $Space$(\mathcal{K} \circ \mathcal{W}^{-1} )$.