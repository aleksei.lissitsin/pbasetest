---
uid: Prnp
slug: rnp
name: RNP
aliases:
  - with the Radon-Nikodym property
---

A Banach space with the Radon-Nikodym property. Such spaces form an injective space ideal.