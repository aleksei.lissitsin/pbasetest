---
uid: Tell_infty-oh
space: Sell_infty
property: Poh
value: false
---
$\ell_\infty$ is not octahedral.