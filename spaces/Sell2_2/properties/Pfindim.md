---
uid: Tell2_2-findim
space: Sell2_2
property: Pfindim
value: true
---

$\ell^2_2$ is finite-dimensional.