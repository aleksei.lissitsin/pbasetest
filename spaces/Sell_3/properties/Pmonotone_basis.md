---
uid: Tell_3-monotone_basis
space: Sell_3
property: Pmonotone_basis
value: true
---
$\ell_3$ has a monotone basis