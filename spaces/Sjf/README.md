---
uid: Sjf
slug: jf
name: JF
aliases:
  - James function space
refs:
  - mr: 390720
    name: Examples of separable spaces which do not contain $\ell_1$ and whose duals are non-separable.
---

James function space.