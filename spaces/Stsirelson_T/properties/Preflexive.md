---
uid: Ttsirelson_T
space: Stsirelson_T
property: Preflexive
value: true
---
Tsirelson space is reflexive.