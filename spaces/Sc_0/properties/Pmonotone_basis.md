---
uid: Tc_0-monotone_basis
space: Sc_0
property: Pmonotone_basis
value: true
---
$c_0$ has a monotone basis