---
uid: Tell_1-nondual-renorming-quasidual
space: Sell_1-nondual-renorming
property: Pquasidual
value: true
---
Quasi-duality is kept under isomorphisms.