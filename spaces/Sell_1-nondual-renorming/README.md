---
uid: Sell_1-nondual-renorming
slug: ell_1-nondual-renorming
name: A non-dual renorming of $\ell_1$
refs:
  - doi: 10.2307/2039469
    name: A Renorming of Nonreflexive Banach Spaces
---
A non dual renorming of $\ell_1$.