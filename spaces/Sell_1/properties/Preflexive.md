---
uid: Tell_1-reflexive
space: Sell_1
property: Preflexive
value: false
---

$\ell_1$ is not reflexive