---
uid: Tell_1-monotone_basis
space: Sell_1
property: Pmonotone_basis
value: true
---
$\ell_1$ has a monotone basis