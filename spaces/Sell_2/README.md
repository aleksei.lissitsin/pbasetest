---
uid: Sell_2
slug: ell_2
name: $\ell_2$
aliases:
  - Hilbert space of sequences
---
A Hilbert space of sequences.