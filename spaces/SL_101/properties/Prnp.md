---
uid: TL_101-rnp
space: SL_101
property: Prnp
value: false
---

$L_1[0,1]$ does not have RNP.