---
uid: TL_101-schur
space: SL_101
property: Pschur
value: false
---
$L_1[0,1]$ is not Schur.