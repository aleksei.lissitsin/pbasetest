---
uid: Tell_2c-separable
space: Sell_2c
property: Pseparable
value: false
---
$\ell_2(c)$ is not separable