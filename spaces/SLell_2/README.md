---
uid: SLell_2
slug: Lell_2
name: $\mathcal L(\ell_2)$
---

The space of bounded linear operators on $\ell_2$