---
uid: Tap_not_map-separable
space: Sap_not_map
property: Pseparable
value: true
---
This space is separable.