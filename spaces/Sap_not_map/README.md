---
uid: Sap_not_map
slug: ap_not_map
name: AP not MAP
---

A separable space (even with a separable dual) having the AP but failing the MAP.
See Example 1.e.20 in Classical Banach Spaces I.