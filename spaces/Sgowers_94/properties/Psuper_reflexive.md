---
uid: Tgowers_94-super_reflexive
space: Sgowers_94
property: Psuper_reflexive
value: false
---
It contains no infinite-dimensional reflexive space.