---
uid: Sgowers_94
slug: gowers_94
name: Gowers space from 1994
refs:
  - mr: 1250820
    name: A Banach space not containing $c_0$, $\ell_1$ or a reflexive subspace.
---

An example by Gowers.