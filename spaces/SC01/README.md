---
uid: SC01
slug: C01
name: $C[0,1]$
---
The universal separable Banach space $C[0,1]$.