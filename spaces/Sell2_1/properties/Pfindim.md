---
uid: Tell2_1-findim
space: Sell2_1
property: Pfindim
value: true
---

$\ell^2_1$ is finite-dimensional.