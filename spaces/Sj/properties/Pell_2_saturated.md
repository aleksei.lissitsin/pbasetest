---
uid: Tj-ell_2_saturated
space: Sj
property: Pell_2_saturated
value: true
---
J is $\ell_2$-saturated.