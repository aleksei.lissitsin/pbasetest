---
uid: Tj-reflexive
space: Sj
property: Preflexive
value: false
---
J is not reflexive.