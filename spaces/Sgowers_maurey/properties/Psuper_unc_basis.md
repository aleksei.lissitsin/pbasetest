---
uid: Tgowers_maurey-super_unc_basis
space: Sgowers_maurey
property: Psuper_unc_basis
value: false
---
It does not contain an unconditional basic sequence.