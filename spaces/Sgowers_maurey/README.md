---
uid: Sgowers_maurey
slug: gowers_maurey
name: Gowers-Maurey space
refs:
  - mr: 1201238
    name: The unconditional basic sequence problem
---

The hereditarily indecomposable space of Gowers and Maurey.